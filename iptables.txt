I.Definition:
Iptables is a command-line firewall utility that uses policy chains to allow or block traffic. When a connection tries to establish itself on your system, iptables looks for a rule in its list to match it to. If it doesn't find one, it resorts to the default action.
II.Types of chains
iptables uses three different chains: input, forward, and output.
-Input: This chain is used to control the behavior for incoming connections. For example, if user attemps to SSH into ypur PC/server, iptables will attempt to match the IP address and port to rule in the input chain.
-Forward: This chain is used for incoming connections that aren't actually being delivered locally. Think of a router - data is always being sent to it but rarely actually destined for the router itself; the data is just forwarded to its targer.Unless you're doing some kind of routing, NATing or something else on your system that requires forwarding, you won't even use this chain.
-Output: This chain is used for outgoing connections. For example, if you try to ping howtogeek.com, iptables will check its output chain to see what the rules are regarding ping and howtogeek.com before making a decision to allow or deny the connection attempt
Set default rule:
iptables --policy INPUT ACCPET/DROP/REJECT
iptables --policy OUTPUT ACCEPT/DROP/REJECT
iptables --policy FORWARD ACCEPT/DROP/REJECT
Responses:
	Accept - Allow the connection
	Drop - Drop the connection, act like it never happend. This is best if you don't want the source to realize your system exists.
	Reject - Don't allow the connection, but send back an error.This is best if you don't want them to know that your firewall blocked them

III.Allow or Blocking Specific Connections
Append rules: iptables -A
Insert a rule: iptables -I [chain] [number]
-Connections from a single IP address:
	iptables -A [chain] -s [source-ip] -j [response]
	ex: iptables -A INPUT -s 10.10.10.10 -j DROP
-Connections from a range of IP addresses
	iptables -A [chain] -s [source-ip]/[subnet-mask] -j [response]
	exp: iptables -A INPUT -s 10.10.10.0/24 -j DROP
		 iptables -A INPUT -s 10.10.10.0/255.255.255.0 -j DROP
-Connections to a specific port:
	iptables -A INPUT -p [protocol] --dport [destination-port] -s [source-ip] -j [response]
	ex: iptables -A INPUT -p tcp --dport ssh -s 10.10.10.10 -j DROP
	

