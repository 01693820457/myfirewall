from PyQt4 import QtCore, QtGui

import sys
import os

import gui
from firewall import Firewall


class FirewallGui(QtGui.QWidget):

    def __init__(self, parent=None):
        super(FirewallGui, self).__init__(parent)
        self.fw = Firewall('my-firewall')
        self._initUI()

    def _initUI(self):
        self.setFixedSize(800, 500)
        self.setWindowTitle("Firewall")

        logo = self.load_logo(220, 150, "logo.png")
        logo.move(500, 30)

        settings = gui.Settings(self.fw, self)
        settings.setGeometry(10, 10, 400, 150)

        view = gui.View(780, 300, self.fw, self)
        view.move(10, 180)
        # view.setGeometry(10, 180, 780, 300)
        view.ruleTab.connect(
            settings, 
            QtCore.SIGNAL("changeStatus"),
            lambda isEnabled: view.ruleTab.setEnabled(isEnabled)
        )
        view.connect(
            settings,
            QtCore.SIGNAL("log"),
            view.log
        )

    def load_logo(self, width, height, img_path):
        path = os.path.join(os.getcwd(), img_path)
        pixmap = QtGui.QPixmap(path)
        pixmap = pixmap.scaled(width, height, QtCore.Qt.KeepAspectRatio)

        logo = QtGui.QLabel(self)
        logo.setPixmap(pixmap)
        return logo


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    firewall = FirewallGui()
    firewall.show()
    sys.exit(app.exec_())