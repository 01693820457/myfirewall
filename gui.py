from PyQt4 import QtCore, QtGui
from firewall import Firewall
from datetime import datetime
import re


class Settings(QtGui.QWidget):

    def __init__(self, firewall, parent=None):
        super(Settings, self).__init__(parent)
        self.fw = firewall
        self._isEnabled = False

        self._loadOnStartUp()
        self._initUI()
        self.setStatus()

    def _initUI(self):
        items = [target.title() for target in Firewall.TARGETS]

        lbStatus = QtGui.QLabel("Status: ", self)
        lbStatus.move(0, 0)

        self.btnStatus = QtGui.QRadioButton(self)
        self.btnStatus.move(100, 0)
        self.btnStatus.toggled.connect(self.changeStatus)

        lbInput = QtGui.QLabel("Input: ", self)
        lbInput.move(0, 35)

        self.cbInput = QtGui.QComboBox(self)
        self.cbInput.addItems(items)
        self.cbInput.setCurrentIndex(
            items.index(self.fw.inputTarget.title())
        )
        self.cbInput.setGeometry(100, 35, 100, 25)
        self.cbInput.currentIndexChanged.connect(self.changeChainRule)
        self.cbInput.connect(
            self,
            QtCore.SIGNAL("changeStatus"),
            lambda isEnabled: self.cbInput.setEnabled(isEnabled)
        )

        lbOutput = QtGui.QLabel("Output: ", self)
        lbOutput.move(0, 70)

        self.cbOutput = QtGui.QComboBox(self)
        self.cbOutput.addItems(items)
        self.cbOutput.setCurrentIndex(
            items.index(self.fw.outputTarget.title())
        )
        self.cbOutput.setGeometry(100, 70, 100, 25)
        self.cbOutput.currentIndexChanged.connect(self.changeChainRule)
        self.cbOutput.connect(
            self,
            QtCore.SIGNAL("changeStatus"),
            lambda isEnabled: self.cbOutput.setEnabled(isEnabled)
        )

        lbForward = QtGui.QLabel("Forward: ", self)
        lbForward.move(0, 105)

        self.cbForward = QtGui.QComboBox(self)
        self.cbForward.addItems(items)
        self.cbForward.setCurrentIndex(
            items.index(self.fw.forwardTarget.title())
        )
        self.cbForward.setGeometry(100, 105, 100, 25)
        self.cbForward.currentIndexChanged.connect(self.changeChainRule)
        self.cbForward.connect(
            self,
            QtCore.SIGNAL("changeStatus"),
            lambda isEnabled: self.cbForward.setEnabled(isEnabled))

    def _loadOnStartUp(self):
        """Run command to get current status"""
        self._isEnabled = self.fw.isEnabled()
        
    def setStatus(self):
        status = "ON" if self._isEnabled else "OFF"
        self.btnStatus.setChecked(self._isEnabled)
        self.btnStatus.setText(status)
        self.emit(QtCore.SIGNAL("changeStatus"), self._isEnabled)
        
        log = "Status: " + status
        self.emit(QtCore.SIGNAL("log"), log)

    def changeStatus(self):
        self._isEnabled = self.btnStatus.isChecked()
        self.fw.enable() if self._isEnabled else self.fw.disable()
        self.setStatus()

    @QtCore.pyqtSlot()
    def changeChainRule(self):
        source = self.sender()
        target = source.currentText().lower()

        if source is self.cbInput:
            chain = "INPUT"
        elif source is self.cbOutput:
            chain = "OUTPUT"
        elif source is self.cbForward:
            chain = "FORWARD"

        self.fw.setRuleMainChain(chain, target)

        log = "Chain {} all {}".format(chain, target.upper())
        self.emit(QtCore.SIGNAL("log"), log)


class View(QtGui.QTabWidget):
    log_file_path = 'log.txt'

    def __init__(self, width, height, firewall, parent=None):
        super(View, self).__init__(parent)
        self.fw = firewall
        self.width = width
        self.height = height
        self.setGeometry(0, 0, width, height)

        self._initUI()
        self._loadOnStartUp()

    def _loadOnStartUp(self):
        self.ruleTab.setEnabled(self.fw.isEnabled())
        self.load_logs()

    def _initUI(self):
        # width = self.frameGeometry().width()
        # height = self.frameGeometry().height()

        self.ruleTab = QtGui.QWidget(self)
        self.ruleView = QtGui.QTableView(self.ruleTab)
        self.ruleView.resize(self.width, self.height-70)
        self.ruleView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.ruleModel = QtGui.QStandardItemModel()
        self.ruleModel.setHorizontalHeaderLabels([
            "Chain",
            "Target",
            "Protocol",
            "Source",
            "Destination",
            "Port",
        ])
        self.ruleView.setModel(self.ruleModel)
        self.ruleView.setColumnWidth(0, 98)
        self.ruleView.setColumnWidth(1, 100)
        self.ruleView.setColumnWidth(2, 90)
        self.ruleView.setColumnWidth(3, 190)
        self.ruleView.setColumnWidth(4, 190)
        self.ruleView.setColumnWidth(5, 90)
        self.ruleModel.appendRow([QtGui.QStandardItem("hello")])
        self.ruleModel.appendRow([QtGui.QStandardItem("hello")])
        self.addTab(self.ruleTab, "Rules")

        self.btnAddRule = QtGui.QPushButton("Add", self.ruleTab)
        self.btnAddRule.setGeometry(self.width-180, self.height-65, 80, 30)
        self.btnAddRule.clicked.connect(self.openAddRuleDialog)
        self.btnDeleteRule = QtGui.QPushButton("Delete", self.ruleTab)
        self.btnDeleteRule.setGeometry(self.width-90, self.height-65, 80, 30)
        self.btnDeleteRule.clicked.connect(self.deleteRule)

        self.logTab = QtGui.QWidget(self)
        self.logView = QtGui.QListView(self.logTab)
        self.logView.resize(self.width, self.height-70)
        self.logView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.logModel = QtGui.QStandardItemModel()
        self.logView.setModel(self.logModel)
        self.addTab(self.logTab, "Logs")

        self.btnClearLog = QtGui.QPushButton("Clear", self.logTab)
        self.btnClearLog.setGeometry(self.width-90, self.height-65, 80, 30)
        self.btnClearLog.clicked.connect(self.clearLog)

    def openAddRuleDialog(self):
        dialog = AddRuleDialog()
        self.connect(dialog, QtCore.SIGNAL("addRule"), self.fw.addRule)
        dialog.exec_()

    def deleteRule(self):
        print("delete rule")

    def log(self, new_log):
        time_format = "[%d/%m/%Y %H:%M] "
        time_now = (datetime.now()).strftime(time_format)
        new_log = time_now + new_log
        # Add log to view
        log_item = QtGui.QStandardItem(new_log)
        self.logModel.appendRow(log_item)

        # Write log to file
        with open(self.log_file_path, 'a') as log_file:
            log_file.write(new_log + '\n')

    def load_logs(self):
        try:
            with open(self.log_file_path, 'r') as log_file:
                logs = log_file.readlines()
                for log in logs:
                    log_item = QtGui.QStandardItem(log.strip())
                    self.logModel.appendRow(log_item)
        except:
            with open(self.log_file_path, 'w') as log_file:
                pass

    def clearLog(self):
        # Remove all from views
        self.logModel = QtGui.QStandardItemModel()
        self.logView.setModel(self.logModel)

        # Remove all from files
        with open(self.log_file_path, 'w') as log_file:
            log_file.write("")


class AddRuleDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(AddRuleDialog, self).__init__(parent)
        self.setWindowTitle("Add New Rule")
        self.width = 450
        self.height = 400
        self.setFixedSize(self.width, self.height)
        self._initUI()

    def _initUI(self):
        self.tabView = QtGui.QTabWidget(self)
        self.tabView.setGeometry(20, 20, self.width - 40, self.height - 80)

        # self._addSimpleTab()
        self.simpleTab = SimpleAddRuleTab(self)
        self.tabView.addTab(self.simpleTab, "Simple")

        self.advancedTab = AdvancedAddRuleTab(self)
        self.tabView.addTab(self.advancedTab, "Advanced")

        self.btnAdd = QtGui.QPushButton("Add", self)
        self.btnAdd.setGeometry(self.width - 170, self.height - 40, 70, 28)
        self.btnAdd.clicked.connect(self.addClicked)

        self.btnClose = QtGui.QPushButton("Close", self)
        self.btnClose.setGeometry(self.width - 90, self.height - 40, 70, 28)
        self.btnClose.clicked.connect(self.close)

    def getRuleParams(self):
        currentTab = self.tabView.currentWidget()
        params = currentTab.getRuleParams()
        return params

    def addClicked(self):
        params = self.getRuleParams()
        if params:
            self.emit(QtCore.SIGNAL("addRule"), params)
            self.close()


class AddRuleTab(QtGui.QWidget):

    def __init__(self, parent):
        super(AddRuleTab, self).__init__(parent)
        targets = [target.title() for target in Firewall.TARGETS]
        width = parent.width - 180
        height = 23

        lbTarget = QtGui.QLabel("Target:", self)
        lbTarget.move(10, 10)

        self.cbTarget = QtGui.QComboBox(self)
        self.cbTarget.addItems(targets)
        self.cbTarget.setGeometry(100, 10, width, height)

        lbChain = QtGui.QLabel("Chain:", self)
        lbChain.move(10, 40)

        self.cbChain = QtGui.QComboBox(self)
        self.cbChain.setGeometry(100, 40, width, height)
        self.cbChain.addItems(["INPUT", "OUTPUT", "FORWARD"])

        lbProtocol = QtGui.QLabel("Protocol:", self)
        lbProtocol.move(10, 70)

        self.cbProtocol = QtGui.QComboBox(self)
        self.cbProtocol.setGeometry(100, 70, width, height)
        self.cbProtocol.addItems(["Both", "Udp", "Tcp"])

    def getRuleParams(self):
        params = {
            "target": str(self.cbTarget.currentText()).upper(),
            "chain": str(self.cbChain.currentText()).upper(),
            "protocol": str(self.cbProtocol.currentText()).lower()
        }
        return params


class SimpleAddRuleTab(AddRuleTab):

    def __init__(self, parent=None):
        super(SimpleAddRuleTab, self).__init__(parent)
        width = parent.width - 180
        height = 23

        lbIp = QtGui.QLabel("IP:", self)
        lbIp.move(10, 100)

        self.txtIp = QtGui.QLineEdit(self)
        self.txtIp.setPlaceholderText("ALL")
        self.txtIp.setGeometry(100, 100, width-90, height)

        self.txtPort = QtGui.QLineEdit(self)
        self.txtPort.setPlaceholderText("PORT")

        self.txtPort.setGeometry(width+20, 100, 80, height)

    def getRuleParams(self):
        params = super(SimpleAddRuleTab, self).getRuleParams()
        params['ip'] = str(self.txtIp.text())
        params['port'] = str(self.txtPort.text())
        
        validated_params = self.validate(params)
        return validated_params

    def validate_port(self, port):
        if port == '':
            raise ValueError("Port is not valid!")
            
        port = int(port)
        if port < 0 or port > 65535:
            raise ValueError("Port is not valid!")

    def validate_ip(self, ip):
        matched = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", ip)
        if matched is None:
            raise ValueError("Ip is not valid!")
    
    def validate(self, params):
        valid_params = dict(params) 
        ip = valid_params.get('ip', None)
        port = valid_params.get('port', None)

        try:
            # validate ip
            if ip:
                self.validate_ip(ip)
            else:
                valid_params.pop('ip')

            if port is not None:
                self.validate_port(port)
        except ValueError as err:
            Warning(str(err)).exec_()
            return None

        return valid_params


class AdvancedAddRuleTab(AddRuleTab):

    def __init__(self, parent):
        super(AdvancedAddRuleTab, self).__init__(parent)

        width = parent.width - 180
        height = 23

        lbInsert = QtGui.QLabel("Insert:", self)
        lbInsert.move(10, 100)

        self.spinInsert = QtGui.QSpinBox(self)
        self.spinInsert.setGeometry(100, 100, width, height)

        lbInterface = QtGui.QLabel("Interface:", self)
        lbInterface.move(10, 130)

        self.cbInterface = QtGui.QComboBox(self)
        self.cbInterface.addItems(["all", "enp3s0", "lo", "wlp2s0"])
        self.cbInterface.setGeometry(100, 130, width, height)

        lbIpFrom = QtGui.QLabel("From:", self)
        lbIpFrom.move(10, 160)

        self.txtIpFrom = QtGui.QLineEdit(self)
        self.txtIpFrom.setPlaceholderText("IP")
        self.txtIpFrom.setGeometry(100, 160, width-90, height)

        self.txtPortFrom = QtGui.QLineEdit(self)
        self.txtPortFrom.setPlaceholderText("PORT")
        self.txtPortFrom.setGeometry(width + 20, 160, 80, height)

        lbIpTo = QtGui.QLabel("To:", self)
        lbIpTo.move(10, 190)

        self.txtPortTo = QtGui.QLineEdit(self)
        self.txtPortTo.setPlaceholderText("PORT")
        self.txtPortTo.setGeometry(width + 20, 190, 80, height)

        self.txtIpTo = QtGui.QLineEdit(self)
        self.txtIpTo.setPlaceholderText("IP")
        self.txtIpTo.setGeometry(100, 190, width-90, height)

    def getRuleParams(self):
        params = super(AdvancedAddRuleTab, self).getRuleParams()
        params["insert"] = str(self.spinInsert.text())
        params["interface"] = str(self.cbInterface.currentText()).lower()
        params["ipFrom"] = str(self.txtIpFrom.text())
        params["ipTo"] = str(self.txtIpTo.text())
        params["portFrom"] = str(self.txtPortFrom.text())
        params["portTo"] = str(self.txtPortTo.text())
        return params


class Warning(QtGui.QDialog):

    def __init__(self, msg, parent=None):
        super(Warning, self).__init__(parent)
        self.setWindowTitle("Warning!")
        self.setFixedSize(300, 100)

        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)

        text = QtGui.QLabel(msg)

        layout.addWidget(text)
        layout.setAlignment(QtCore.Qt.AlignCenter)
        

        
