import subprocess


class Firewall:
    TARGETS = ['ACCEPT', 'REJECT', 'DROP']
    CHAINS = ['INPUT', 'OUTPUT', 'FORWARD']

    def __init__(self, name):
        self._name = name
        self._isEnabled = self.isEnabled()
        self.inputTarget = ''
        self.outputTarget = ''
        self.forwardTarget = ''

        self._loadCurrentRules()

    def _loadCurrentRules(self):
        output = self._run('sudo iptables -L -v')
        for line in output.split('\n'):
            if 'Chain INPUT' in line:
                self.inputTarget = self._findTarget(line)
            elif 'Chain OUTPUT' in line:
                self.outputTarget = self._findTarget(line)
            elif 'Chain FORWARD' in line:
                self.forwardTarget = self._findTarget(line)

    def _findTarget(self, chain):
        target = ''
        for t in Firewall.TARGETS:
            if t in chain:
                target = t
                break
        return target

    def enable(self):
        if not self._isEnabled:
            for chain in self.CHAINS:
                self._createChain(chain)
            self._isEnabled = True
    
    def disable(self):
        if self._isEnabled:
            for chain in self.CHAINS:
                self._deleteChain(chain)
            self._isEnabled = False

    def isEnabled(self):
        output = self._run('sudo iptables -L -v')
        num_appear = output.count(self._name)
        return num_appear == (len(self.CHAINS)*2)

    def setRuleMainChain(self, chain, target):
        if chain not in Firewall.CHAINS:
            raise ValueError("inValid main chain")
        
        target = target.upper()
        if target not in Firewall.TARGETS:
            raise ValueError("Invalid target")

        command = "sudo iptables -P {} {}".format(chain, target)
        self._run(command)

    def _run(self, command):
        # print(command)
        p = subprocess.Popen(
            command.split(),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT
        )

        output = ''
        for line in iter(p.stdout.readline, b''):
            output += line.decode('utf-8')
        # print(output, end="\n\n")
        return output

    def _get_real_chain(self, chainName):
        return self._name + '-' + chainName

    def _createChain(self, chainName):
        refChain = chainName
        chainName = self._get_real_chain(chainName)
        # Create new chain
        self._run('sudo iptables -N {}'.format(chainName))
        # Reference new chain with main chain
        self._run('sudo iptables -A {} -j {}'.format(refChain, chainName))

    def _deleteChain(self, chainName):
        refChain = chainName
        chainName = self._name + '-' + chainName
        # Delete reference from main chain
        self._run('sudo iptables -D {} -j {}'.format(refChain, chainName))
        # Delete all rule in chain
        self._run('sudo iptables -F {}'.format(chainName))
        # Delete chain
        self._run('sudo iptables -X {}'.format(chainName))

    def addRule(self, params):
        protocol = params.get("protocol", None)
        if protocol == 'both':
            # Run for both protocol
            udp_params = dict(params)
            udp_params['protocol'] = 'udp'
            self.addRule(udp_params)

            tcp_params = dict(params)
            tcp_params['protocol'] = 'tcp'
            self.addRule(tcp_params)
        else:
            command = "sudo iptables "

            target = params.get("target")
            chain = self._get_real_chain(params.get("chain"))  
            command += "-A {} ".format(chain)

            protocol = params.get("protocol")
            command += "-p {} ".format(protocol)

            # For simple add
            port = params.get("port", None)
            ip = params.get("ip", None)
            if ip:
                command += "-s {} ".format(ip)
            if port:
                command += "--dport {} ".format(port)
        
            command += "-j {} ".format(target)
            print(command)
            print(self._run(command))


    